import st3m.run, leds, bl00mbox, uos

from st3m.application import Application, ApplicationContext
from st3m.input import InputState, InputController
from ctx import Context

class turr3t(Application):
    input: InputController
    channel: bl00mbox.Channel
    samples: list[bl00mbox.sampler] = []
    sample_names: list[str] = [
        "excuseme.wav",
        "hate.wav",
        "notgood.wav",
        "ohmy.wav",
        "whothere.wav"
    ]
    is_loading: bool = True

    def __init__(self, context):
        super().__init__(context)
        try:
            self.bundle_path = context.bundle_path
        except:
            self.bundle_path = "/flash/sys/apps/Schmoaaaaah-turr3t"
        #self.bundle_path = "/flash/sys/apps/Schmoaaaaah-turr3t"
        sourcepath = self.bundle_path+"/samples/"
        destinationpath = "/flash/sys/samples/"
        for name in self.sample_names:
            try:
                uos.stat(destinationpath+name)
            except:
                uos.rename(sourcepath+name,destinationpath+name)
        leds.set_brightness(255)
        self.input = InputController()
        self.channel = bl00mbox.Channel("turr3t")
        self.current_frame = 1
        self.max_frames = 24
        self.interacted: bool = False
        self.interacteddelta = 0
        self.redtime = 1000

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        if self.is_loading:
            self._load_next_sample()
            return
        
        self.interacteddelta += delta_ms
        
        if self.interacted and self.interacteddelta >= self.redtime:
            for i in range(40):
                if i <= 19:
                    leds.set_rgb(i,255,100,0)
                if i > 20:
                    leds.set_rgb(i,0,0,255)
            self.interacted = False

        if self.input.captouch.petals[2].whole.pressed:
            leds.set_all_rgb(255, 0, 0)
            self.interacted = True
            self.interacteddelta = 0
            self.redtime = 1000
            self.samples[0].signals.trigger.start()
        if self.input.captouch.petals[4].whole.pressed:
            leds.set_all_rgb(255, 0, 0)
            self.interacted = True
            self.interacteddelta = 0
            self.redtime = 900
            self.samples[1].signals.trigger.start()
        if self.input.captouch.petals[6].whole.pressed:
            leds.set_all_rgb(255, 0, 0)
            self.interacted = True
            self.interacteddelta = 0
            self.redtime = 1200
            self.samples[2].signals.trigger.start()
        if self.input.captouch.petals[8].whole.pressed:
            leds.set_all_rgb(255, 0, 0)
            self.interacted = True
            self.interacteddelta = 0
            self.redtime = 1000
            self.samples[3].signals.trigger.start()
        if self.input.captouch.petals[0].whole.pressed:
            leds.set_all_rgb(255, 0, 0)
            self.interacted = True
            self.interacteddelta = 0
            self.redtime = 800
            self.samples[4].signals.trigger.start()

        leds.update()
        
    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        # Loop if we printed the last frame
        if self.current_frame > self.max_frames:
                self.current_frame = 1
        # Faster
        theframe = self.bundle_path+"/frames/turr_"+str(self.current_frame)+".png"

        # Draw frame
        ctx.image(theframe,-120,-120,240,240)

        # Increase frame
        self.current_frame += 1

    def _load_next_sample(self) -> None:
        loaded = len(self.samples)
        if loaded >= len(self.sample_names):
            self.is_loading = False
            return

        i = loaded

        sample = self.channel.new(bl00mbox.patches.sampler, self.sample_names[i - 1])
        sample.signals.output = self.channel.mixer

        self.samples.append(sample)


if __name__ == "__main__":
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(turr3t(ApplicationContext()))
